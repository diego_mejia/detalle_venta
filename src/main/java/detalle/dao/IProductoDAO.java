package detalle.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import detalle.model.Producto;

@Repository
public interface IProductoDAO extends JpaRepository<Producto, Integer>{

}
