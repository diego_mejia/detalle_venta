package detalle.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import detalle.model.Persona;

@Repository
public interface IPersonaDAO extends JpaRepository<Persona, Integer>{

}
