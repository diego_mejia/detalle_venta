package detalle.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import detalle.model.Venta;

@Repository
public interface IVentaDAO extends JpaRepository<Venta, Integer>{

}
